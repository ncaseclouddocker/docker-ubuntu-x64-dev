# [ubuntu-x64-dev](https://hub.docker.com/r/forumi0721ubuntux64/ubuntu-x64-dev/)
[![](https://images.microbadger.com/badges/version/forumi0721ubuntux64/ubuntu-x64-dev.svg)](https://microbadger.com/images/forumi0721ubuntux64/ubuntu-x64-dev "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721ubuntux64/ubuntu-x64-dev.svg)](https://microbadger.com/images/forumi0721ubuntux64/ubuntu-x64-dev "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721ubuntux64/ubuntu-x64-dev.svg?style=flat-square)](https://hub.docker.com/r/forumi0721ubuntux64/ubuntu-x64-dev/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721ubuntux64/ubuntu-x64-dev.svg?style=flat-square)](https://hub.docker.com/r/forumi0721ubuntux64/ubuntu-x64-dev/)



----------------------------------------
#### Description
* Distribution : [Ubuntu](https://www.ubuntu.com/)
* Architecture : x64
* Base Image   : [forumi0721ubuntux64/ubuntu-x64-base](https://hub.docker.com/r/forumi0721ubuntux64/ubuntu-x64-base/)
* Appplication : -
    - Development environment



----------------------------------------
#### Run
```sh
docker run -i -t --rm \
        -e USER_NAME=<user_name> \
        forumi0721ubuntux64/ubuntu-x64-dev:latest
```


###### Notes
* Default user name : forumi0721



----------------------------------------
#### Docker Options
| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports
| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes
| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables
| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| USER_NAME          | login username (default:forumi0721)              |



----------------------------------------
* [forumi0721ubuntux64/ubuntu-x64-dev](https://hub.docker.com/r/forumi0721ubuntux64/ubuntu-x64-dev/)
* [forumi0721ubuntuarmv7h/ubuntu-armv7h-dev](https://hub.docker.com/r/forumi0721ubuntuarmv7h/ubuntu-armv7h-dev/)

